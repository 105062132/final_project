var mainState ={
    preload : function () {

        game.load.spritesheet('soundicons', 'asset/soundicons.png', 40, 40)
        game.load.spritesheet('player', 'asset/player.png', 32, 32);
        //game.load.spritesheet('background', 'asset/bg.png', 400, 355);
        game.load.image('background', 'asset/bg.png');
        game.load.image('ceiling', 'asset/ceiling.png');
        game.load.image('normal', 'asset/normal.png');
        game.load.image('nails', 'asset/nails.png');
        game.load.spritesheet('conveyorRight', 'asset/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'asset/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'asset/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'asset/fake.png', 96, 36);
        game.load.image('pixel', 'asset/red.jpg');
        game.load.image('undo', 'asset/undo.png');
        game.load.image('pause', 'asset/pause.png');
        game.load.image('life', 'asset/life.png');
        game.load.spritesheet('yellow', 'asset/yellow.jpg', 6, 13.5);
        game.load.audio('powerup', 'sound/powerup.mp3');
        game.load.audio('powerdown', 'sound/powerdown.mp3');
        game.load.audio('jump', 'sound/jump.mp3');
    },

    create : function  () {

        this.platforms = [];
        this.players = [];
        this.location=605;
        this.yellows= [];
        this.lastTime = 0;
        this.distance = 0;
        this.score = 0;

        this.cursor = game.input.keyboard.addKeys({
            'esc': Phaser.Keyboard.ESC,
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT
        });


        //audio
        powerup = game.add.audio('powerup');
        powerdown = game.add.audio('powerdown');
        jump = game.add.audio('jump');


        //background lifebar
        //this.background2 = game.add.sprite(0, 0, 'background2');
    /*    this.background = game.add.sprite(0, 0, 'background');
        this.life = game.add.sprite(500, 10, 'life');
        for(var i=0;i<12;i++){
            var yellow = game.add.sprite(509+i*8, 33, 'yellow');
            this.yellows.push(yellow);
        }*/

        
        //UI
        var Button = game.add.button(500 , 330,  "undo", this.stateMenu, this);
        Button.anchor.set(0.5);
        Button = game.add.button(580 , 330, "pause", this.statePause, this);
        Button.frame = 1;
        Button.anchor.set(0.5);

        //sound
        /*var soundButton = game.add.button(500 , 400,  "soundicons", this.setSound1, this);
        soundButton.anchor.set(0.5);
        soundButton = game.add.button(550 , 400, "soundicons", this.setSound2, this);
        soundButton.frame = 1;
        soundButton.anchor.set(0.5);*/


        //createWall;
        /*this.bar1 = game.add.image(26, 50, 'bar1');
        this.leftWall = game.add.sprite(33, 50, 'wall');
        game.physics.arcade.enable(this.leftWall);
        this.leftWall.body.immovable = true;

        this.bar4 = game.add.image(450, 49, 'bar1');
        this.rightWall = game.add.sprite(433, 50, 'wall');
        game.physics.arcade.enable(this.rightWall);
        this.rightWall.body.immovable = true;

        this.bar2 = game.add.image(26, 40, 'bar2');
        this.bar3 = game.add.image(28, 404, 'bar3');
        this.ceiling = game.add.image(50, 50, 'ceiling');*/


        //createPlayer
        /*this.player = game.add.sprite(200, 50, 'player');
        this.player.direction = 10;
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 500;
        this.player.animations.add('left', [0, 1, 2, 3], 8);
        this.player.animations.add('right', [9, 10, 11, 12], 8);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player.animations.add('fly', [36, 37, 38, 39], 12);
        this.player.life = 12;
        this.player.touchOn = undefined;*/

        var button = game.add.button(50 , 600, 'sodier1', this.createrPlayer1);
        button = game.add.button(150, 600, 'sodier2', this.createrPlayer2);
        button = game.add.button(250, 600, 'sodier3', this.createrPlayer3);
        button = game.add.button(350, 600, 'sodier4', this.createrPlayer4);
        //createText
        var style1 = {fill: 'orange', fontSize: '30px'}
        var style2 = {fill: 'red', fontSize: '30px'}
        this.text = game.add.text(180, 5, '', style1);
        this.text2 = game.add.text(180, 200, '遊戲暫停', style2);
        this.text2.visible = false;
    },

    update : function () {
        this.physics.arcade.collide(this.players, this.platforms, this.effect);

        //this.ceilingTouch(this.player);
        this.GameOver();
        this.updateText();

        this.movePlayers();
        this.createEnemies();
        this.moveEnemies();
    },
    setSound1:function() {
        playSound = true; 
    },
    setSound2:function() {
        playSound = false;
    },
    stateMenu: function() {
        game.state.start('menu');
    },
    statePause: function() {
        if(game.paused){
            game.paused=false;
            this.text2.visible = false;
        }
        else{
            game.paused=true;
            this.text2.visible = true;
        }
    },
    createPlayer1:function  () {
        var player;
        var x = 100;
        var y = 500;

        player = game.add.sprite(x, y, 'normal');
        player.life=3;
        
        game.physics.arcade.enable(player);
        this.players.push(player);
    },
    createPlayer2:function  () {
        var player;
        var x = 100;
        var y = 500;

        platform = game.add.sprite(x, y, 'nails');
        player.life=3;
        
        game.physics.arcade.enable(player);
        this.players.push(player);
    },
    createPlayer3:function  () {
        var player;
        var x = 100;
        var y = 500;

        player = game.add.sprite(x, y, 'trampoline');
        player.life=3;
        
        game.physics.arcade.enable(player);
        this.players.push(player);
    },
    createPlayer4:function  () {
        var player;
        var x = 100;
        var y = 500;

        player = game.add.sprite(x, y, 'fake');
        player.life=3;
        
        game.physics.arcade.enable(player);
        this.players.push(player);
    },
    movePlayers:function  () {
        for(var i=0; i<this.players.length; i++) {
            var player = this.players[i];
            player.body.position.x += 1.2;
            if(player.body.position.x <= 40||player.body.life==0) {
                player.destroy();
                this.players.splice(i, 1);
            }
        }
    },
        
    createEnemies : function  () {
        var i=this.platforms.length-1;
        var platform = this.platforms[i];
        /*if(i<0){
            this.lastTime = game.time.now;
            this.createPlatform();
            this.distance += 1;
            if(this.distance%4==0)
            {
                this.score+=1;
            }
        }*/
        if(game.time.now > this.lastTime + 1500) {
            this.lastTime = game.time.now;
            this.createPlatform();
            this.distance += 1;
            if(this.distance%4==0)
            {
                this.score+=1;
            }
        }
    },

    createEnemy : function () {

        var enemy;
        var x = 1400;
        var y = 500;
        //var random = Math.random() * 100;

        enemy = game.add.sprite(x, y, 'normal');
        enemy.life=3;
        /*} 
        else if (random>=45 && random < 60) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } 
        else if (random>=60 && random < 70) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } 
        else if (random>=70 && random < 80) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } 
        else if (random>=80 && random < 90) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } 
        else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }*/

        game.physics.arcade.enable(enemy);
        /*platform.body.immovable = true;*/
        this.platforms.push(enemy);

        /*platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;*/
    },

    /*movePlayer : function () {
        if(this.cursor.left.isDown) {
            this.player.body.velocity.x = -250;
        } 
        else if(this.cursor.right.isDown) {
            this.player.body.velocity.x = 250;
        } 
        else {
            this.player.body.velocity.x = 0;
        }

        //Animate
        var x = this.player.body.velocity.x;
        var y = this.player.body.velocity.y;

        if (x < 0 && y > 0) {
            this.player.animations.play('flyleft');
        }
        else if (x > 0 && y > 0) {
            this.player.animations.play('flyright');
        }
        else if (x < 0 && y == 0) {
            this.player.animations.play('left');
        }
        else if (x > 0 && y == 0) {
            this.player.animations.play('right');
        }
        else if (x == 0 && y != 0) {
            this.player.animations.play('fly');
        }
        else if (x == 0 && y == 0) {
            this.player.frame = 8;
        }
    },*/

    moveEnemies : function () {
        for(var i=0; i<this.platforms.length; i++) {
            var enemy = this.platforms[i];
            enemy.body.position.x -= 1.2;
            if(enemy.body.position.x <= 40||enemy.body.life==0) {
                enemy.destroy();
                this.platforms.splice(i, 1);
            }
        }
    },

    updateText : function () {
        if(this.score<10){
            this.text.setText('地下000'+ this.score +'階')
        }
        else if(this.score<100){
            this.text.setText('地下00'+ this.score +'階')
        }
        else if(this.score<1000){
            this.text.setText('地下0' + this.score +'階')
        }
        else{
            this.text.setText('地下' + this.score +'階')
        }
    },

    effect : function (player, enemy) {
        if(enemy.key == 'conveyorRight') {
            player.body.x += 2;
        }
        else if(enemy.key == 'conveyorLeft') {
            player.body.x -= 2;
        }
        else if(enemy.key == 'trampoline') {
            platform.animations.play('jump');
            player.body.velocity.y = -270;
            if(player.life < 12) {
                player.life += 1;
                var yellow = game.add.sprite(mainState.location, 33, 'yellow');
                mainState.yellows[player.life-1]=yellow;
                mainState.location+=8;
            }
            if(playSound){
                jump.play();
            }
        }
        else if(enemy.key == 'nails') {
            if (player.touchOn !== platform) {
                player.touchOn = platform;
                if(playSound){
                    powerdown.play();
                }
                mainState.location-=32; 
                var i;
                for(var i=player.life-1;i>player.life-5;i--)
                {
                    var yellow = mainState.yellows[i];
                    if(i>=0){
                        yellow.destroy();
                    }
                }
                player.life-=4;  
                game.camera.flash(0xff0000, 100);

                //particle
                var x=player.body.position.x,
                    y=player.body.position.y;

                mainState.emitter = game.add.emitter(x, y, 10);
                mainState.emitter.makeParticles('pixel');
                mainState.emitter.setYSpeed(-150, 150);
                mainState.emitter.setXSpeed(-150, 150);
                mainState.emitter.setScale(2, 0, 2, 0, 800);
                mainState.emitter.gravity = 500;  
                mainState.emitter.start(true, 800, null, 10);
                         
            }
        }
        else if(platform.key == 'normal') {
            if (player.touchOn !== platform) {
                if(player.life < 12) {
                    player.life += 1;
                    var yellow = game.add.sprite(mainState.location, 33, 'yellow');
                    mainState.yellows[player.life-1]=yellow;
                    mainState.location+=8;
                    if(playSound){
                        powerup.play();
                    }
                }

                player.touchOn = platform;
            }
        }
        else if(platform.key == 'fake') {
            if(player.touchOn !== platform) {
                platform.animations.play('turn');
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                player.touchOn = platform;
            }
        }
    },
    /*ceilingTouch : function (player) {
        if(player.body.y < 50) {

            //particle
            var x=player.body.position.x,
                y=player.body.position.y;

            this.emitter = game.add.emitter(x, y, 10);
            this.emitter.makeParticles('pixel');
            this.emitter.setYSpeed(-150, 150);
            this.emitter.setXSpeed(-150, 150);
            this.emitter.setScale(2, 0, 2, 0, 800);
            this.emitter.gravity = 500;  
            this.emitter.start(true, 800, null, 10);

            player.body.y+=30;
            if(player.body.velocity.y < 0) {
                player.body.velocity.y = 0;
            }
        
            this.location-=32; 
            var i;
            for(var i=player.life-1;i>player.life-5;i--)
            {
                var yellow = mainState.yellows[i];
                if(i>=0){
                    yellow.destroy();
                }
            }
            player.life-=4; 
            game.camera.flash(0xff0000, 100);
            if(playSound){
                powerdown.play();
            }
        }
    },*/

    GameOver : function () {
        /*if(this.player.life <= 0 || this.player.body.y > 400) {
            this.platforms.forEach(function(s) {s.destroy()});
            this.platforms = [];
            game.state.start('end');
        }*/
    }
}
game.state.add('main', mainState);