var count=0,money=0,experience=0;
var mainState4 ={
    preload : function () {
        game.load.spritesheet('sodier1', 'asset/sodier1.png', 100, 140);
        game.load.spritesheet('sodier2', 'asset/sodier2.png', 100, 140);
        game.load.spritesheet('sodier3', 'asset/sodier3.png', 100, 140);
        game.load.spritesheet('sodier4', 'asset/sodier4.png', 100, 140);

        game.load.spritesheet('enemy1', 'asset/enemy1.png', 100, 140);
        game.load.spritesheet('enemy2', 'asset/enemy2.png', 100, 140);
        game.load.spritesheet('enemy3', 'asset/enemy3.png', 100, 140);
        game.load.spritesheet('enemy4', 'asset/enemy4.png', 100, 140);
        game.load.image('power', 'asset/power.png');

        game.load.image('head1', 'asset/head1.png');
        game.load.image('head2', 'asset/head2.png');
        game.load.image('head3', 'asset/head3.png');
        game.load.image('head4', 'asset/head4.png');
        game.load.image('head5', 'asset/head5.png');

        game.load.image('background', 'asset/bg.png');
        game.load.image('tower', 'asset/tower.png');//tower
        game.load.image('pixel', 'asset/red.jpg');
    },

    create : function  () {

        this.enemies = [];
        this.players = [];
        this.lastTime = 0;
        this.startTime=game.time.now;
        this.distance = 0;
        this.score = 0;
        this.flag=0;
        money = 0;

        this.cursor = game.input.keyboard.addKeys({
            'esc': Phaser.Keyboard.ESC,
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT
        });

        this.background = game.add.sprite(0, 0, 'background');
        //UI
        var Button = game.add.button(80 , 600,  "head1", this.createPlayer1, this);
        Button.anchor.set(0.5);
        Button = game.add.button(280 , 600, "head2", this.createPlayer2, this);
        Button.anchor.set(0.5);
        Button = game.add.button(480 , 600, "head3", this.createPlayer3, this);
        Button.anchor.set(0.5);
        Button = game.add.button(680 , 600, "head4", this.createPlayer4, this);
        Button.anchor.set(0.5);
        Button = game.add.button(880 , 600, "head5", this.speedup, this);
        Button.anchor.set(0.5);

        //create tower
        this.tower1 = game.add.sprite(20, 300, 'tower');
        game.physics.arcade.enable(this.tower1);
        this.tower1.body.immovable = true;
        this.tower1.life=50;
        this.tower2 = game.add.sprite(1350, 300, 'tower');
        game.physics.arcade.enable(this.tower2);
        this.tower2.body.immovable = true;
        this.tower2.life=80;

        //createText
        var style1 = {fill: 'orange', fontSize: '30px'}
        var style2 = {fill: 'red', fontSize: '30px'}
        this.text1 = game.add.text(105, 250, '', style1);
        this.text2 = game.add.text(1435 ,250, '', style1);
        this.text3 = game.add.text(180, 200, '遊戲暫停', style2);
        this.text3.visible = false;
        this.text4 = game.add.text(105, 20, '', style1);

        this.text = game.add.text(30, 530, '', style1);
        this.text.setText('$30');
        this.text = game.add.text(230, 530, '', style1);
        this.text.setText('$50');
        this.text = game.add.text(430, 530, '', style1);
        this.text.setText('$20');
        this.text = game.add.text(630, 530, '', style1);
        this.text.setText('$80');
        this.text = game.add.text(830, 530, '', style1);
        this.text.setText('$200(可預支)');
         //firebase
         var postsRef = firebase.database().ref('com_list/' + player);
         postsRef.once('value', function(snapshot){
             var scoreText = 'Level:';
             //snapshot.forEach(function(childSnapshot){
                 var exists = (snapshot.val() !== null);   
                 if(exists) {
                     lv1 = snapshot.val().level1;
                     lv2 = snapshot.val().level2;
                     lv3 = snapshot.val().level3;
                     lv4 = snapshot.val().level4;
                     //level5 = snapshot.val().level5;
                     exp = snapshot.val().exp;
                     console.log(level1);
                 }
                 else{
                     
                     lv1 = 1;
                     lv2 = 1;
                     lv3 =1;
                     lv4 = 1;
                     //level5 = 1;
                     exp = 0;
                 }
         });
 
         //
    },

    update : function () {
        this.physics.arcade.collide(this.players, this.enemies, this.effect);
        this.physics.arcade.collide(this.tower1, this.enemies, this.attack1);
        this.physics.arcade.collide(this.tower2, this.players, this.attack2);
        count++;
        if(this.flag==0){
            if(count%10==0){
                money++;
            }
        }
        else if(this.flag==1){
            if(count%7==0){
                money++;
            }
        }
        
        this.GameOver();
        this.updateText();

        this.movePlayers();
        this.createEnemies();
        this.moveEnemies();
    },
    setSound1:function() {
        playSound = true; 
    },
    setSound2:function() {
        playSound = false;
    },
    stateMenu: function() {
        game.state.start('menu');
    },
    statePause: function() {
        if(game.paused){
            game.paused=false;
            this.text3.visible = false;
        }
        else{
            game.paused=true;
            this.text3.visible = true;
        }
    },
    speedup:function() {
        if(this.flag==0){
            money-=200; 
            this.flag=1;
            this.power = game.add.sprite(180, 390, 'power');
        }
        
    },
    createPlayer1:function  () {
        var player;
        var x = 150;
        var y = 320;
        if(money>=30){
            player = game.add.sprite(x, y, 'sodier1');
            player.animations.add('walk', [0, 1], 4);
            player.life=5 + lv1 - 1;
            player.cdTime=0;
            game.physics.arcade.enable(player);
            this.players.push(player);
            money-=30;
        }
       
    },
    createPlayer2:function  () {
        var player;
        var x = 150;
        var y = 320;
        if(money>=50){
            player = game.add.sprite(x, y, 'sodier2');
            player.animations.add('walk', [0, 1], 3);
            player.life=12 + lv2 - 1;
            player.cdTime=0;
            game.physics.arcade.enable(player);
            this.players.push(player);
            money-=50;
        }
    },
    createPlayer3:function  () {
        var player;
        var x = 150;
        var y = 320;
        if(money>=20){
            player = game.add.sprite(x, y, 'sodier3');
            player.animations.add('walk', [0, 1], 8);
            player.life=3 + lv3 - 1;
            player.cdTime=0;
            game.physics.arcade.enable(player);
            this.players.push(player);
            money-=20;
        }
    },
    createPlayer4:function  () {
        var player;
        var x = 150;
        var y = 320;
        if(money>=80){
            player = game.add.sprite(x, y, 'sodier4');
            player.animations.add('walk', [0, 1], 2);
            player.life=15 + lv4 - 1;
            player.cdTime=0;
            game.physics.arcade.enable(player);
            this.players.push(player);
            money-=80;
        }
    },
    movePlayers:function  () {
        for(var i=0; i<this.players.length; i++) {
            var player = this.players[i];
            if(player.key=='sodier1'){
                player.body.position.x += 1.2;
            }
            else if(player.key=='sodier2'){
                player.body.position.x += 0.8;
            }
            else if(player.key=='sodier3'){
                player.body.position.x += 2.4;
            }
            else if(player.key=='sodier4'){
                player.body.position.x += 0.6;
            }
            player.animations.play('walk');
            if(player.life==0) {
                player.destroy();
                this.players.splice(i, 1);
            }
        }
    },
        
    createEnemies : function  () {

        if(game.time.now > this.lastTime + 5500) {
            this.lastTime = game.time.now;
            this.createEnemy();
        }
    },

    createEnemy : function () {

        var enemy;
        var x = 1400;
        var y = 320;
        if(game.time.now <= this.startTime + 110000) {
            enemy = game.add.sprite(x, y, 'enemy1');
            enemy.animations.add('walk', [0, 1], 4);
            enemy.life=8;
        }
        else if(game.time.now <= this.startTime + 137500 && game.time.now > this.startTime + 110000) {
            enemy = game.add.sprite(x, y, 'enemy2');
            enemy.animations.add('walk', [0, 1], 3);
            enemy.life=15;
        }
        else if(game.time.now <= this.startTime + 165000 && game.time.now > this.startTime + 137500) {
            enemy = game.add.sprite(x, y, 'enemy3');
            enemy.animations.add('walk', [0, 1], 8);
            enemy.life=6;
        }
        else if(game.time.now <= this.startTime + 176000 && game.time.now > this.startTime + 165000) {
            enemy = game.add.sprite(x, y, 'enemy4');
            enemy.animations.add('walk', [0, 1], 2);
            enemy.life=18;
        }
        else if (game.time.now > this.startTime + 176000){
            this.startTime=game.time.now;
        }
        enemy.scale.x*=-1;
        game.physics.arcade.enable(enemy);
        this.enemies.push(enemy);
    },

    moveEnemies : function () {
        for(var i=0; i<this.enemies.length; i++) {
            var enemy = this.enemies[i];
            enemy.body.position.x -= 1.2;
            enemy.animations.play('walk');
            if(enemy.life==0) {
                if(enemy.key=='enemy1'){
                    money+=5;
                    experience+=13;
                }
                else if(enemy.key=='enemy1'){
                    money+=8;
                    experience+=18;
                }
                else if(enemy.key=='enemy1'){
                    money+=3;
                    experience+=11;
                }
                else if(enemy.key=='enemy1'){
                    money+=10;
                    experience+=23;
                }
                enemy.destroy();
                this.enemies.splice(i, 1);
            }
        }
    },

    updateText : function () {
        
        this.text1.setText(this.tower1.life+ '/50');
        this.text2.setText(this.tower2.life+ '/80');
        this.text4.setText('金錢：'+ money);
    },

    effect : function (player, enemy) {
        enemy.body.x += 50;
        enemy.life-=1;
        player.life-=1;
        if(player.key == 'sodier1') {
            player.body.x -=50;
        }
        if(player.key == 'sodier3') {
            player.body.x -=50;
        }
        if(player.key == 'sodier4') {
            player.body.x -=50;
        }
        
    },
    attack1 : function (tower, sodier) {
        sodier.body.x +=50;
        tower.life-=1;
    },
    attack2 : function (tower, sodier) {
        sodier.body.x -= 50;
        tower.life-=1;
        
    },

    GameOver : function () {
        if(this.tower1.life <= 0 ) {
            this.tower1.destroy();
            game.state.start('end');
        }
        else if(this.tower2.life <= 0 ) {
            experience+=50;
            this.tower2.destroy();
            game.state.start('end');
        }
    }
}
game.state.add('main4', mainState4);