var select,powerup,powerdown,jump,
    playSound=true;
var player;
var level1, level2, level3, level4, level5, exp;
var val;
var postsRef;
var count=0,money=0,experience=0;
var lv1, lv2, lv3, lv4;
var menuState ={
    preload: function() {
        game.load.image('background', 'asset/bg.png');
        game.load.audio('select', 'sound/select.mp3');
    },
    
    create: function() {
        this.background = game.add.sprite(0, 0, 'background');
        select = game.add.audio('select');
        var style = {
            font: "48px Monospace",
            fill: "#00ff00",
            align: "center"
        };
        var text = game.add.text(800, 230, '新。台灣價值', style);
        text.anchor.set(0.5);

        var text_start = game.add.text(700, 300, '開始遊戲', { font: '50px Arial', fill: '#000' });
        text_start.inputEnabled = true;
        text_start.events.onInputUp.add(this.startGame);
        var text_board = game.add.text(700, 400, '兵種升級', { font: '50px Arial', fill: '#000' });
        text_board.inputEnabled = true;
        text_board.events.onInputUp.add(this.score);
        /*var text_end = game.add.text(700, 500, 'end', { font: '50px Arial', fill: '#000' });
        text_end.inputEnabled = true;
        text_end.events.onInputUp.add(this.end);*/
        if(player == null)
            player = prompt("Please enter your name", "");
        console.log(player);
        postsRef = firebase.database().ref('com_list/' + player);
        /*if (player != null)
        {
            postsRef = firebase.database().ref('com_list/' + player);
            if ()
        }*/
    },
    end: function(target){
        game.state.start('end');
    },

    startGame: function(target) {
        game.state.start('select');
    },
    score: function(target) {
        if(playSound){
            select.play();
        }
        game.state.start('score');
    }
}

var game = new Phaser.Game(1600, 650, Phaser.AUTO, 'canvas');
game.state.add('menu', menuState);
game.state.start('menu');