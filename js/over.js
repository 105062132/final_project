var endState ={
    preload:function(){
        game.load.image('background', 'asset/bg.jpg');
    },
    create: function() {
        var style = {
            font: "48px Monospace",
            fill: "#00ff00",
            align: "center"
        };
        var style2 = {
            font: "30px Monospace",
            fill: "#00ff00",
            align: "center"
        };
        this.background = game.add.sprite(0, 0, 'background');
        //var player = prompt("Please enter your name", "");
        /*if(player!=''){
            var newpostref = firebase.database().ref('list').push();
                newpostref.set({
                    score: -mainState.score,
                    name: player
                });
        }*/

        var text1 = game.add.text(690, 250, '遊戲結束', style);
        var text2 = game.add.text(640, 330, '獲取經驗值:'+experience, style);

        var text_start = game.add.text(620, 450, 'again', { font: '50px Arial', fill: '#000' });
        text_start.inputEnabled = true;
        text_start.events.onInputUp.add(this.play);
        var text_board = game.add.text(850, 450, 'menu', { font: '50px Arial', fill: '#000' });
        text_board.inputEnabled = true;
        text_board.events.onInputUp.add(this.menu);
        //firebase
        /*postsRef = firebase.database().ref('com_list/' + player);
        postsRef.once('value', function(snapshot){
            var scoreText = 'Level:';
            //snapshot.forEach(function(childSnapshot){
                var exists = (snapshot.val() !== null);   
                if(exists) {
                    level1 = snapshot.val().level1;
                    level2 = snapshot.val().level2;
                    level3 = snapshot.val().level3;
                    level4 = snapshot.val().level4;
                    level5 = snapshot.val().level5;
                    exp = snapshot.val().exp;
                    console.log(level1);
                }
                else{
                    
                    level1 = 1;
                    level2 = 1;
                    level3 =1;
                    level4 = 1;
                    level5 = 1;
                    exp = experience;
                }
        });
        postsRef.set({
            level1: level1,
            level2: level2,
            level3: level3,
            level4: level4,
            level5: level5,
            exp: 500
        });*/
    },
    play: function () {
        if(playSound){
            select.play();
        }
        //firebase
        var postsRef = firebase.database().ref('com_list/' + player);
        postsRef.once('value', function(snapshot){
            var scoreText = 'Level:';
            //snapshot.forEach(function(childSnapshot){
                var exists = (snapshot.val() !== null);   
                if(exists) {
                    level1 = snapshot.val().level1;
                    level2 = snapshot.val().level2;
                    level3 = snapshot.val().level3;
                    level4 = snapshot.val().level4;
                    level5 = snapshot.val().level5;
                    exp = snapshot.val().exp;
                    console.log(level1);
                }
                else{
                    
                    level1 = 1;
                    level2 = 1;
                    level3 =1;
                    level4 = 1;
                    level5 = 1;
                    exp = 0;
                }
        });
        postsRef.set({
            level1: level1,
            level2: level2,
            level3: level3,
            level4: level4,
            level5: level5,
            exp: exp + experience
        });
        //
        game.state.start('main1');
        
    },
    menu: function () {
        if(playSound){
            select.play();
        }
        //firebase
        var Ref = firebase.database().ref('com_list/' + player);
        Ref.once('value', function(snapshot){
            var scoreText = 'Level:';
            //snapshot.forEach(function(childSnapshot){
                var exists = (snapshot.val() !== null);   
                if(exists) {
                    level1 = snapshot.val().level1;
                    level2 = snapshot.val().level2;
                    level3 = snapshot.val().level3;
                    level4 = snapshot.val().level4;
                    level5 = snapshot.val().level5;
                    exp = snapshot.val().exp;
                    console.log(level1);
                }
                else{
                    
                    level1 = 1;
                    level2 = 1;
                    level3 =1;
                    level4 = 1;
                    level5 = 1;
                    exp = 0;
                }
        });
        Ref.set({
            level1: level1,
            level2: level2,
            level3: level3,
            level4: level4,
            level5: level5,
            exp: exp + experience
        });
        //
        game.state.start('menu');
    }

}
game.state.add('end', endState);