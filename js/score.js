
var scoreState ={
    preload: function() {
        game.load.image('head1', 'asset/head1.png');
        game.load.image('head2', 'asset/head2.png');
        game.load.image('head3', 'asset/head3.png');
        game.load.image('head4', 'asset/head4.png');
        game.load.image('head5', 'asset/head5.png');
        game.load.image('background', 'asset/bg.png');
        game.load.audio('select', 'sound/select.mp3');
    },
    
    create: function() {
        this.background = game.add.sprite(0, 0, 'background');

        postsRef = firebase.database().ref('com_list/' + player);
        //UI
        var Button = game.add.button(80 , 600,  "head1", this.upgradePlayer1, this);
        Button.anchor.set(0.5);
        Button = game.add.button(280 , 600, "head2", this.upgradePlayer2, this);
        Button.anchor.set(0.5);
        Button = game.add.button(480 , 600, "head3", this.upgradePlayer3, this);
        Button.anchor.set(0.5);
        Button = game.add.button(680 , 600, "head4", this.upgradePlayer4, this);
        Button.anchor.set(0.5);
        Button = game.add.button(880 , 600, "head5", this.speedup, this);
        Button.anchor.set(0.5);
        
        
        //createText
        var style1 = {fill: 'orange', fontSize: '30px'}
        var style2 = {fill: 'red', fontSize: '30px'}
        this.text1 = game.add.text(30, 530, '', style1);
        //this.text.setText('$30');
        this.text2 = game.add.text(230, 530, '', style1);
        //this.text.setText('$50');
        this.text3 = game.add.text(430, 530, '', style1);
        //this.text.setText('$20');
        this.text4 = game.add.text(630, 530, '', style1);
        //this.text.setText('$80');
        this.text5 = game.add.text(830, 530, '', style1);
        this.text6 = game.add.text(1400, 50, '', style1);
        //this.initplayer();
        //this.updateText();
        //this.text.setText('$200(可預支)');
        /*var leaderboard = game.add.text(232, game.height/2+30,'loading...' , { font: '25px Arial', fill: '#ffffff' });
        leaderboard.anchor.setTo(0.5, 0.5);

        var post = firebase.database().ref('list').orderByChild('score').limitToFirst(6);
        post.once('value', function(snapshot){
            var text1 = '';
            snapshot.forEach(function(childSnapshot){
                text1 += childSnapshot.val().name + '  ' +( -childSnapshot.val().score) + '\n';
            });
            leaderboard.text = text1;
        });*/

        var text6 = game.add.text(400, 250, 'Back', { font: '35px Arial', fill: '#000' });
        text6.inputEnabled = true;
        text6.events.onInputUp.add(this.back);
        var text7 = game.add.text(500, 250, '查詢', { font: '35px Arial', fill: '#000' });
        text7.inputEnabled = true;
        text7.events.onInputUp.add(this.updateText);
    },
    /*update: function() {
        this.updateText();
    },*/

    initplayer: function() {
        postsRef.set({
            level1: 1,
            level2: 1,
            level3: 1,
            level4: 1,
            level5: 1,
        });
    },

    updateText: function() {

        postsRef.once('value', function(snapshot){
            var scoreText = 'Level:';
            //snapshot.forEach(function(childSnapshot){
                var exists = (snapshot.val() !== null);   
                if(exists) {
                    level1 = snapshot.val().level1;
                    level2 = snapshot.val().level2;
                    level3 = snapshot.val().level3;
                    level4 = snapshot.val().level4;
                    level5 = snapshot.val().level5;
                    exp = snapshot.val().exp;
                    console.log(level1);
                }
                else{
                    postsRef.set({
                        level1: 1,
                        level2: 1,
                        level3: 1,
                        level4: 1,
                        level5: 1,
                        exp: 0
                    });
                    level1 = 1;
                    level2 = 1;
                    level3 =1;
                    level4 = 1;
                    level5 = 1;
                    exp = 0;
                }
            
                /*this.text1.setText(level1);
                this.text2.setText(level2);
                this.text3.setText(level3);
                this.text4.setText(level4);
                this.text5.setText(level5);*/
                //scoreText += childSnapshot.val().name + '  ' + childSnapshot.val().data + '\n';
            //});
            //text6.setText(scoreText);
        });
        scoreState.text1.setText('LV.'+level1 + '$100');
        scoreState.text2.setText('LV.'+level2 + '$200');
        scoreState.text3.setText('LV.'+level3 + '$300');
        scoreState.text4.setText('LV.'+level4 + '$400');
        scoreState.text5.setText('LV.'+level5 + '$500');
        scoreState.text6.setText('經驗值:'+exp);
    },

    upgradePlayer1: function() {
        if (exp > 100){
            level1 +=1;
            exp -= 100;
            postsRef.set({
                level1: level1,
                level2: level2,
                level3: level3,
                level4: level4,
                level5: level5,
                exp: exp
            });
        }
        
        this.updateText();
    },

    upgradePlayer2: function() {
        if (exp > 200){
            level2 +=1;
            exp -= 200;
            postsRef.set({
                level1: level1,
                level2: level2,
                level3: level3,
                level4: level4,
                level5: level5,
                exp: exp
            });
        }
        this.updateText();
    },

    upgradePlayer3: function() {
        if (exp > 300){
            level3 +=1;
            exp -= 300;
            postsRef.set({
                level1: level1,
                level2: level2,
                level3: level3,
                level4: level4,
                level5: level5,
                exp: exp
            });
        }
        this.updateText();
    },

    upgradePlayer4: function() {
        if (exp > 400){
            level4 +=1;
            exp -= 400;
            postsRef.set({
                level1: level1,
                level2: level2,
                level3: level3,
                level4: level4,
                level5: level5,
                exp: exp
            });
        }
        this.updateText();
    },

    speedup: function() {
        level5 +=1;
        postsRef.set({
            level1: level1,
            level2: level2,
            level3: level3,
            level4: level4,
            level5: level5,
            exp: exp
        });
        this.updateText();
    },
    
    back: function(target) {
        
        game.state.start('menu');
    }
}
game.state.add('score', scoreState);