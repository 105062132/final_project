
var selectState ={
    preload: function() {
        game.load.image('background', 'asset/bg.png');
    },
    
    create: function() {
        this.background = game.add.sprite(0, 0, 'background');

        var text1 = game.add.text(600, 100, '關卡1', { font: '50px Arial', fill: '#000' });
        text1.inputEnabled = true;
        text1.events.onInputUp.add(this.startGame1);
        var text2 = game.add.text(600, 160, '關卡2', { font: '50px Arial', fill: '#000' });
        text2.inputEnabled = true;
        text2.events.onInputUp.add(this.startGame2);
        var text3 = game.add.text(600, 220, '關卡3', { font: '50px Arial', fill: '#000' });
        text3.inputEnabled = true;
        text3.events.onInputUp.add(this.startGame3);
        var text4 = game.add.text(600, 280, '關卡4', { font: '50px Arial', fill: '#000' });
        text4.inputEnabled = true;
        text4.events.onInputUp.add(this.startGame4);
        var text5 = game.add.text(600, 340, '關卡5', { font: '50px Arial', fill: '#000' });
        text5.inputEnabled = true;
        text5.events.onInputUp.add(this.startGame5);
        var text6 = game.add.text(700, 450, 'back', { font: '50px Arial', fill: '#000' });
        text6.inputEnabled = true;
        text6.events.onInputUp.add(this.back);
    },
    
    startGame1: function(target) {
        game.state.start('main1');
    },
    startGame2: function(target) {
        game.state.start('main2');
    },
    startGame3: function(target) {
        game.state.start('main3');
    },
    startGame4: function(target) {
        game.state.start('main4');
    },
    startGame5: function(target) {
        game.state.start('main5');
    },
    back: function(target) {
        game.state.start('menu');
    }
}
game.state.add('select', selectState);